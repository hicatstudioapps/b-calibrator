package com.hc.batterycalibrator;

import android.app.Application;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.multidex.MultiDexApplication;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.onesignal.OneSignal;
//import com.onesignal.OneSignal;


public class SpotTheCatApp extends MultiDexApplication {


    public static  InterstitialAd interstitialAd;
    AdRequest request;
    public static Typeface face;
    public static Typeface faceR;
    public static SharedPreferences preferences;



   public void onCreate() {
      super.onCreate();
       OneSignal.startInit(getBaseContext()).init();
      ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).build();
      ImageLoader.getInstance().init(config);
      interstitialAd=new InterstitialAd(this);
      interstitialAd.setAdUnitId("ca-app-pub-1502760063615827/2944506317");
       request= new AdRequest.Builder().build();
      interstitialAd.setAdListener(new AdListener() {
          @Override
          public void onAdClosed() {
              interstitialAd.loadAd(request);
          }
      });
      interstitialAd.loadAd(request);
//        if(getSharedPreferences("data",MODE_PRIVATE).getBoolean(Constants.CONTENT_READY,false))
      face= Typeface.createFromAsset(getAssets(),"rl.ttf");
      // faceR= Typeface.createFromAsset(getAssets(),"R.OTF");

   }

		public static void showInterstitial(){
			if(interstitialAd.isLoaded()){
				interstitialAd.show();
			}
		}
}
