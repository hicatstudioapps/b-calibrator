package com.hc.batterycalibrator;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.io.IOException;

import badabing.lib.ServerUtilities;
import badabing.lib.apprater.AppRater;

public class MainActivity extends AppCompatActivity {


    String batteryLevelInfo = "Battery Level";
    ImageButton calibrateButton = null;
    int level = 0;
    Button likeitButton = null;
    TextView manual = null;
    Button moreButton = null;
    TextView textBatteryLevel = null;
    Button upgradeButton = null;
    AlertDialog.Builder alertbox;
    AlertDialog.Builder alertfail;
    AlertDialog.Builder alertok;
    boolean beep;
    MediaPlayer beepsound;
    int blevel;
    int bvoltage;
    boolean firstbeep;
    boolean force100;
    String statfile = "batterystats.bin";
    String statpath;
    private BroadcastReceiver battery_receiver = new BroadcastReceiver() {
        public void onReceive(Context var1, Intent var2) {
            Bundle b= var2.getExtras();
            String var9 = var2.getStringExtra("technology");
            int var7 = var2.getIntExtra("plugged", -1);
            int var5 = var2.getIntExtra("scale", -1);
            int var4 = var2.getIntExtra("health", 0);
            int var8 = var2.getIntExtra("status", 0);
            int var3 = var2.getIntExtra("level", -1);
            MainActivity.this.blevel = var3;
            int var6 = var2.getIntExtra("temperature", 0) / 10;
            var2.getExtras();
            if (true) {
                if (var3 >= 0 && var5 > 0) {
                    MainActivity.this.level = var3 * 100 / var5;
                }

                String var10 = getString(R.string.level) + " " + MainActivity.this.level + "%\n";
                var9 = var10 + getString(R.string.tech) + " " + var9 + "\n";
//                var9 = var9 + "Plugged: " + MainActivity.this.getPlugTypeString(var7) + "\n";
                var9 = var9 + getString(R.string.health) + " " + MainActivity.this.getHealthString(var4) + "\n";
//                var9 = var9 + "Status: " + MainActivity.this.getStatusString(var8) + "\n";
                var9 = var9 + getString(R.string.temp) + " " + var6 + " C";
                MainActivity.this.setBatteryLevelText(var9);
                ((TextView)MainActivity.this.findViewById(R.id.charge)).setText(var10);
                ((TextView)MainActivity.this.findViewById(R.id.charge)).setTypeface(SpotTheCatApp.face);

            } else {
                MainActivity.this.setBatteryLevelText("Battery not present!!!");
            }
            if (MainActivity.this.blevel >= 100 && MainActivity.this.beep && MainActivity.this.firstbeep) {
                MainActivity.this.firstbeep = false;
                MainActivity.this.beepsound.start();
            }
        }
    };
    private View.OnClickListener calibrateButton_handle = new View.OnClickListener() {
        public void onClick(View var1) {
            final ProgressDialog pd = new ProgressDialog(MainActivity.this);
            pd.setMessage(getString(R.string.delete) + " " + statfile + "...");
            pd.setCancelable(false);
            boolean force = force100;
            boolean beepp = beep;
            if (MainActivity.this.blevel < 100 && MainActivity.this.force100) {
                MainActivity.this.alertbox.show();
            } else if (MainActivity.this.superuser("rm " + MainActivity.this.statpath)) {

                pd.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pd.hide();
                        MainActivity.this.alertok.show();
                    }
                }, 2000);
                //   MainActivity.this.alertok.show();
                SpotTheCatApp.showInterstitial();
            } else {
                SpotTheCatApp.showInterstitial();
                MainActivity.this.alertfail.show();
            }
            // var3.show();
        }
    };
    private View.OnClickListener likeitButton_handle = new View.OnClickListener() {
        public void onClick(View var1) {
            Intent var2 = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.ignisoft.battery.calibration"));
            MainActivity.this.startActivity(var2);
        }
    };
    private View.OnClickListener moreButton_handle = new View.OnClickListener() {
        public void onClick(View var1) {
            Intent var2 = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/developer?id=Igni+Software"));
            MainActivity.this.startActivity(var2);
        }
    };
    private View.OnClickListener upgradeButton_handle = new View.OnClickListener() {
        public void onClick(View var1) {
            Intent var2 = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.ignisoft.battery.calibration.noads"));
            MainActivity.this.startActivity(var2);
        }
    };
    private AdView adView;
    private AdRequest var13;
    private TextView akkuszint;
    private BroadcastReceiver mBatInfoReceiver;

    private String getHealthString(int var1) {
        String var2 = "Unknown";
        switch (var1) {
            case 2:
                var2 = "Good";
                break;
            case 3:
                var2 = "Over Heat";
                break;
            case 4:
                var2 = "Dead";
                break;
            case 5:
                var2 = "Over Voltage";
                break;
            case 6:
                var2 = "Failure";
        }

        return var2;
    }

    private String getPlugTypeString(int var1) {
        String var2 = "Unknown";
        switch (var1) {
            case 1:
                var2 = "AC";
                break;
            case 2:
                var2 = "USB";
        }

        return var2;
    }

    private String getStatusString(int var1) {
        String var2 = "Unknown";
        switch (var1) {
            case 2:
                var2 = "Charging";
                break;
            case 3:
                var2 = "Discharging";
                break;
            case 4:
                var2 = "Not Charging";
                break;
            case 5:
                var2 = "Full";
        }

        return var2;
    }

    private void registerBatteryLevelReceiver() {
        IntentFilter var1 = new IntentFilter("android.intent.action.BATTERY_CHANGED");
        this.registerReceiver(this.battery_receiver, var1);
    }

    private void setBatteryLevelText(String var1) {
        this.textBatteryLevel.setTypeface(SpotTheCatApp.face);
        this.textBatteryLevel.setText(var1);
    }

    public void onCreate(Bundle var1) {
        this.statpath = "/data/system/" + this.statfile;
        this.beep = true;
        this.firstbeep = true;
        this.force100 = false;
        this.blevel = 0;
        this.bvoltage = 0;
        this.beepsound = MediaPlayer.create(this, R.raw.beep1);
        super.onCreate(var1);
        this.setContentView(R.layout.main);
        this.adView = (AdView) this.findViewById(R.id.adView);
        var13 = (new AdRequest.Builder()).build();
        this.adView.loadAd(var13);
        this.adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
            }
        });
        this.getSharedPreferences("firstTime", 0).edit().putBoolean("showDialog", false).commit();
        this.textBatteryLevel = (TextView) this.findViewById(R.id.batterylevel_text);
        this.manual = (TextView) this.findViewById(R.id.instructionText);
        this.manual.setTypeface(SpotTheCatApp.face);
        ((TextView)findViewById(R.id.textView2)).setTypeface(SpotTheCatApp.face);
        this.registerBatteryLevelReceiver();
        this.calibrateButton = (ImageButton) this.findViewById(R.id.calibrateButton);
        this.likeitButton = (Button) this.findViewById(R.id.likeitButton);
        this.moreButton = (Button) this.findViewById(R.id.moreButton);
        this.calibrateButton.setOnClickListener(this.calibrateButton_handle);
        this.likeitButton.setOnClickListener(this.likeitButton_handle);
        this.moreButton.setOnClickListener(this.moreButton_handle);
        this.manual.setText(getString(R.string.step_1) + "\n" + getString(R.string.step_2) + "\n" + getString(R.string.step_3) + "\n" + getString(R.string.step_4) + "\n");
        if (!(new File(this.statpath)).exists()) {
            this.makealert(getString(R.string.warning) + "\n" + this.statfile + getString(R.string.warning_1)).show();
        } else if (!this.superuser("")) {
            this.makealert(getString(R.string.root)).show();
        }
        this.alertbox = this.makealert(getString(R.string.string_1));
        this.alertok = this.makealert(getString(R.string.ok)).setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showRate();
            }
        });
        this.alertfail = this.makealert(getString(R.string.fail));
//        ToggleButton t1= (ToggleButton)findViewById(R.id.toggleButton);
//        t1.setTextOff(getString(R.string.wait));
//        t1.setTextOn(getString(R.string.wait));

    }

    private void showRate() {
        AlertDialog.Builder dialog1 = new AlertDialog.Builder(MainActivity.this);
        dialog1.setTitle(getString(R.string.app_name)).setMessage(getString(R.string.please_rate_2))
                .setPositiveButton(getString(R.string.rate_app), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AppRater.rateNow(MainActivity.this);
                    }
                }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog1.show();
    }

    private AlertDialog.Builder makealert(String var1) {
        AlertDialog.Builder var2 = new AlertDialog.Builder(this);
        var2.setMessage(var1);
        var2.setNeutralButton("Ok", (DialogInterface.OnClickListener) null);
        return var2;
    }

    private boolean superuser(String var1) {
        boolean var3 = true;

        int var2;
        try {
            var2 = Runtime.getRuntime().exec(new String[]{"su", "-c", var1}).waitFor();
        } catch (IOException var4) {
            var3 = false;
            return var3;
        } catch (InterruptedException var5) {
            var3 = false;
            return var3;
        }

        if (var2 != 0) {
            var3 = false;
        }

        return var3;
    }

    public void BeepIfCharged(View var1) {
        this.beep = ((ToggleButton) var1).isChecked();
    }

    public void Force100Handler(View var1) {
        this.force100 = ((ToggleButton) var1).isChecked();
    }

    protected void onDestroy() {
        this.unregisterReceiver(this.battery_receiver);
        super.onDestroy();
    }

}
