package com.lacostra.utils.notification;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;

import com.rey.material.app.SimpleDialog;

import badabing.lib.apprater.AppRater;

/**
 * Created by CQ on 15/03/2016.
 */
public class Rate {

    public static void rate(final Context context, String mensaje,int buttonColor){
        SharedPreferences pref= context.getSharedPreferences("app",Context.MODE_PRIVATE);
        final SimpleDialog simple =new SimpleDialog(context);
        boolean first=pref.getBoolean("first",true);
        if(first){
            pref.edit().putBoolean("first",false).commit();
            return;
        }
        else{
            if(pref.getBoolean("rate",false))
                return;
            else {
                simple.message(mensaje).positiveAction("Rate")
                        .negativeAction("Cancel").positiveActionClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppRater.rateNow(context);simple.dismiss();
                    }
                }).negativeActionClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        simple.dismiss();
                    }
                }).positiveActionTextColor(buttonColor).negativeActionTextColor(buttonColor).show();
            }
        }
    }
}
