package com.lacostra.utils.notification;

import android.content.Context;
import android.content.Intent;

/**
 * Created by CQ on 19/02/2016.
 */
public class Share {
    public static void shareApp(Context context, String text){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }
}
