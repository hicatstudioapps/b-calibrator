package com.lacostra.utils.notification;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by CQ on 12/02/2016.
 */
public class EnableInteristialServices extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        getSharedPreferences("interistial",MODE_PRIVATE).edit().putBoolean("interistial",true).commit();
        return super.onStartCommand(intent, flags, startId);
    }
}
